# AlarmManager

_Hinweis: Dies ist ein privates Projekt. Die angelegten Einsätze haben nicht in der Realität stattgefunden und dienen lediglich zur Demonstration der Anwendung._

[Zur Live-Demo ->](https://alarm-manager-1283b.web.app/)

## Kurzbeschreibung

Webanwendung für die Dokumentation von Feuerwehreinsätzen im Ehrenamt.

## Verwendete Technologien

- React.js
- Material-UI Kit
- Redux Toolkit
- React Router
- Firebase Database
- Firebase Hosting
