import { createSlice } from "@reduxjs/toolkit";
import {
  fetschAlarmsAsync,
  addAlarmAsync,
  deleteAlarmByIdAsync,
} from "./actions";

const initialState = {
  alarms: [],
  status: "idle",
  error: null,
};

export const alarmSlice = createSlice({
  name: "alarm",
  initialState,
  reducers: {
    resetStatus: (state) => {
      state.status = "running";
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetschAlarmsAsync.pending, (state) => {
        state.status = "fetching";
      })
      .addCase(fetschAlarmsAsync.fulfilled, (state, action) => {
        state.status = "fetchingSucceeded";
        state.alarms = action.payload;
      })
      .addCase(fetschAlarmsAsync.rejected, (state, action) => {
        state.status = "fetchingFailed";
        state.error = action.payload;
      })
      .addCase(addAlarmAsync.pending, (state) => {
        state.status = "adding";
      })
      .addCase(addAlarmAsync.fulfilled, (state, action) => {
        state.status = "addingSucceeded";
        state.alarms.push(action.meta.arg);
      })
      .addCase(addAlarmAsync.rejected, (state, action) => {
        state.status = "addingFailed";
        state.error = action.payload;
      })
      .addCase(deleteAlarmByIdAsync.pending, (state) => {
        state.status = "deleting";
      })
      .addCase(deleteAlarmByIdAsync.fulfilled, (state, action) => {
        state.status = "deletingSucceeded";
        state.alarms = state.alarms.filter(
          (alarm) => alarm.storeId !== action.meta.arg
        );
      })
      .addCase(deleteAlarmByIdAsync.rejected, (state, action) => {
        state.status = "deletingFailed";
        state.error = action.payload;
      });
  },
});

export const { resetStatus } = alarmSlice.actions;

export const selectAlarms = (state) => state.alarm.alarms;

export const selectStatus = (state) => state.alarm.status;

export const selectError = (state) => state.alarm.error;

export default alarmSlice.reducer;
