import { createAsyncThunk } from "@reduxjs/toolkit";
import { db } from "../firebase/firebase";
import {
  collection,
  getDocs,
  setDoc,
  deleteDoc,
  doc,
} from "firebase/firestore";

export const fetschAlarmsAsync = createAsyncThunk(
  "alarm/fetschAlarmsAsync",
  async (_, { rejectWithValue }) => {
    try {
      const querySnapshot = await getDocs(collection(db, "alarms"));
      const data = querySnapshot.docs.map((doc) => doc.data());
      return data;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const addAlarmAsync = createAsyncThunk(
  "alarm/addAlarmAsync",
  async (newAlarm, { rejectWithValue }) => {
    try {
      await setDoc(doc(db, "alarms", newAlarm.storeId), newAlarm);
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const deleteAlarmByIdAsync = createAsyncThunk(
  "alarm/deleteAlarmByIdAsync",
  async (storeId, { rejectWithValue }) => {
    try {
      await deleteDoc(doc(db, "alarms", storeId));
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);
