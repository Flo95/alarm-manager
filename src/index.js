import React from "react";
import ReactDOM from "react-dom/client";

import { createBrowserRouter, RouterProvider } from "react-router-dom";

import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { deDE } from "@mui/x-date-pickers/locales";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import "dayjs/locale/de";

import { store } from "./store/store";
import { Provider } from "react-redux";

import "./index.css";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";

import ErrorPage from "./pages/ErrorPage";

import Login from "./pages/Login";
import Alarms from "./pages/Alarms";
import NewAlarm from "./pages/NewAlarm";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Login />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/uebersicht",
    element: <Alarms />,
  },
  {
    path: "/neuer-einsatz",
    element: <NewAlarm />,
  },
]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <LocalizationProvider
        dateAdapter={AdapterDayjs}
        localeText={
          deDE.components.MuiLocalizationProvider.defaultProps.localeText
        }
        adapterLocale="de"
      >
        <RouterProvider router={router} />
      </LocalizationProvider>
    </Provider>
  </React.StrictMode>
);
