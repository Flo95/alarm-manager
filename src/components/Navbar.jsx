import { Box, Container, Stack, Typography } from "@mui/material";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <Box component="header">
      <Container maxWidth="lg" sx={{ p: 2, borderBottom: "2px solid red" }}>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
        >
          <Typography variant="h3" color="grey">
            EinsatzManager
          </Typography>
          <nav>
            <Stack direction="row" alignItems="center" spacing={2}>
              <Link to="/uebersicht">
                <Typography
                  variant="h5"
                  sx={{
                    color: "grey",
                    cursor: "pointer",
                    opacity: 0.8,
                    "&:hover": {
                      color: "red",
                    },
                  }}
                >
                  Übersicht
                </Typography>
              </Link>
              <Link to="/neuer-einsatz">
                <Typography
                  variant="h5"
                  sx={{
                    color: "grey",
                    cursor: "pointer",
                    opacity: 0.8,
                    "&:hover": {
                      color: "red",
                    },
                  }}
                >
                  Einsatz anlegen
                </Typography>
              </Link>
            </Stack>
          </nav>
        </Stack>
      </Container>
    </Box>
  );
}

export default Navbar;
