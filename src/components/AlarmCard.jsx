import { Box, Stack, Typography, Button } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

import dayjs from "dayjs";

import { useDispatch } from "react-redux";
import { deleteAlarmByIdAsync } from "../store/actions";

function transformDate(date) {
  return dayjs(date).format("DD.MM.YYYY HH:mm");
}

function AlarmCard({ alarm }) {
  const dispatch = useDispatch();
  const { art, nummer, start, ende, einheit, stichwort, ort, inBereitschaft } =
    alarm;

  return (
    <Box
      sx={{
        width: "100%",
        display: "flex",
        flexDirection: "column",
        border: "2px solid black",
        borderRadius: 1,
        boxShadow: 1,
        backgroundColor: "white",
        p: 1,
      }}
    >
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        width="100%"
      >
        <Typography variant="h6" color="grey" fontStyle="italic">
          {`${art} ${nummer} / ${einheit}`}
        </Typography>
        <Typography variant="h6" color="grey" fontStyle="italic">
          {`Beginn: ${transformDate(start)} Uhr`}
        </Typography>
      </Stack>

      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        width="100%"
      >
        <Typography variant="h5" color="black">
          {`${stichwort}, ${ort}`}
        </Typography>
        <Typography variant="h6" color="grey" fontStyle="italic">
          {`Ende: ${transformDate(ende)} Uhr`}
        </Typography>
      </Stack>

      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        gap={2}
        width="100%"
      >
        {inBereitschaft ? (
          <Typography variant="h6" color="grey" fontStyle="italic">
            in Bereitschaft
          </Typography>
        ) : (
          <Stack
            direction="row"
            justifyContent="flex-start"
            alignItems="center"
            gap={3}
            width="100%"
          >
            <Typography variant="h6" color="black">
              {`${alarm?.fahrzeug}`}
            </Typography>
            <Typography variant="h6" color="black">
              {`${alarm?.funktion}`}
            </Typography>
            {alarm?.pa && (
              <Typography variant="h6" color="grey" fontStyle="italic">
                {`PA-Dauer: ${alarm?.paDauer} min`}
              </Typography>
            )}
          </Stack>
        )}
        <Button
          variant="outlined"
          color="error"
          startIcon={<DeleteIcon />}
          size="small"
          onClick={() => dispatch(deleteAlarmByIdAsync(alarm.storeId))}
        >
          Löschen
        </Button>
      </Stack>
    </Box>
  );
}

export default AlarmCard;
