import { Stack } from "@mui/material";

function CardContainer({ children }) {
  return (
    <Stack
      sx={{
        flexDirection: "column",
        alignItems: "center",
        gap: { xs: 1, sm: 2 },
        width: "100%",
        maxHeight: "calc(100vh - 190px)",
        overflow: "auto",
        my: 2,
      }}
    >
      {children}
    </Stack>
  );
}

export default CardContainer;
