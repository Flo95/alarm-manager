import { Stack } from "@mui/material";

function FormRow({ children }) {
  return (
    <Stack
      sx={{
        flexDirection: { xs: "column", sm: "row" },
        alignItems: { xs: "flex-start", sm: "center" },
        justifyContent: "space-between",
        gap: { xs: 2, sm: 4 },
        width: "100%",
        my: 2,
      }}
    >
      {children}
    </Stack>
  );
}

export default FormRow;
