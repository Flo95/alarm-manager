import { useState } from "react";
import {
  Stack,
  Typography,
  ButtonGroup,
  Button,
  TextField,
  MenuItem,
  Switch,
} from "@mui/material";
import InputAdornment from "@mui/material/InputAdornment";

import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import dayjs from "dayjs";

import { nanoid } from "@reduxjs/toolkit";

import FormRow from "./FormRow";

import { einheiten, fahrzeuge, funktionen } from "../utils/dataForSelects";

const erforderlicheFelder = [
  "art",
  "nummer",
  "start",
  "ende",
  "einheit",
  "stichwort",
  "ort",
];
const fahrzeugFunktion = ["fahrzeug", "funktion"];

function isValidData(data) {
  return data.toString().trim().length > 0;
}

function formatToStringWithoutSpaces(data) {
  return data.toString().trimStart().trimEnd();
}

function AlarmForm({ onSubmit }) {
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);

  const [errors, setErrors] = useState({
    nummer: {
      hasError: false,
      message: null,
    },
    einheit: {
      hasError: false,
      message: null,
    },
    stichwort: {
      hasError: false,
      message: null,
    },
    ort: {
      hasError: false,
      message: null,
    },
    fahrzeug: {
      hasError: false,
      message: null,
    },
    funktion: {
      hasError: false,
      message: null,
    },
    paDauer: {
      hasError: false,
      message: null,
    },
  });

  const [formData, setFormData] = useState({
    art: "Einsatzart wählen",
    inBereitschaft: false,
    pa: false,
  });

  function resetStatesAndErrors() {
    setFormData({
      art: "Einsatzart wählen",
      inBereitschaft: false,
      pa: false,
    });
    setStartTime(null);
    setEndTime(null);
    setErrors({
      nummer: {
        hasError: false,
        message: null,
      },
      einheit: {
        hasError: false,
        message: null,
      },
      stichwort: {
        hasError: false,
        message: null,
      },
      ort: {
        hasError: false,
        message: null,
      },
      fahrzeug: {
        hasError: false,
        message: null,
      },
      funktion: {
        hasError: false,
        message: null,
      },
      paDauer: {
        hasError: false,
        message: null,
      },
    });
  }

  function handleSubmit(event) {
    event.preventDefault();

    let newAlarm = {
      storeId: nanoid(),
      inBereitschaft: formData?.inBereitschaft,
    };

    let newErrors = {};

    if (formData?.art === "Einsatzart wählen") {
      newErrors = {
        ...newErrors,
        nummer: {
          hasError: true,
          message: "Einsatzart auswählen!",
        },
      };
    }

    if (formData?.inBereitschaft) {
      erforderlicheFelder.forEach((key) => {
        if (formData[key] && isValidData(formData[key])) {
          const newValue = formatToStringWithoutSpaces(formData[key]);
          newAlarm = { ...newAlarm, [key]: newValue };
        } else {
          newErrors = {
            ...newErrors,
            [key]: {
              hasError: true,
              message: "Pflichtfeld.Ungültige Eingabe",
            },
          };
        }
      });
    } else {
      erforderlicheFelder.forEach((key) => {
        if (formData[key] && isValidData(formData[key])) {
          const newValue = formatToStringWithoutSpaces(formData[key]);
          newAlarm = { ...newAlarm, [key]: newValue };
        } else {
          newErrors = {
            ...newErrors,
            [key]: {
              hasError: true,
              message: "Pflichtfeld. Ungültige Eingabe",
            },
          };
        }
      });
      fahrzeugFunktion.forEach((key) => {
        if (formData[key] && isValidData(formData[key])) {
          const newValue = formatToStringWithoutSpaces(formData[key]);
          newAlarm = { ...newAlarm, [key]: newValue };
        } else {
          newErrors = {
            ...newErrors,
            [key]: {
              hasError: true,
              message: "Pflichtfeld. Ungültige Eingabe",
            },
          };
        }
      });

      newAlarm = { ...newAlarm, pa: formData?.pa };
      if (formData?.pa) {
        if (formData?.paDauer && isValidData(formData?.paDauer)) {
          const newValue = formatToStringWithoutSpaces(formData?.paDauer);
          newAlarm = { ...newAlarm, paDauer: newValue };
        } else {
          newErrors = {
            ...newErrors,
            paDauer: {
              hasError: true,
              message: "Pflichtfeld. Ungültige Eingabe",
            },
          };
        }
      }
    }

    setErrors(newErrors);

    if (
      newErrors?.nummer?.hasError ||
      newErrors?.einheit?.hasError ||
      newErrors?.stichwort?.hasError ||
      newErrors?.ort?.hasError ||
      newErrors?.fahrzeug?.hasError ||
      newErrors?.funktion?.hasError ||
      newErrors?.paDauer?.hasError
    ) {
      return;
    } else {
      onSubmit(newAlarm);
      resetStatesAndErrors();
    }
  }

  function updateFormData(key, value) {
    setFormData({
      ...formData,
      [key]: value,
    });
  }

  function handleChange(event) {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
    setErrors({
      ...errors,
      [name]: {
        hasError: false,
        message: null,
      },
    });
  }

  function handleSwitchChange(event) {
    const { name, checked } = event.target;
    setFormData({ ...formData, [name]: checked });
  }

  return (
    <form onSubmit={handleSubmit}>
      <FormRow>
        <Stack
          direction="row"
          spacing={2}
          alignItems="center"
          sx={{ width: { xs: "100%", sm: "50%" } }}
        >
          <Typography variant="h6">Einsatzart</Typography>
          <ButtonGroup>
            <Button
              variant={formData?.art === "B" ? "contained" : "outlined"}
              onClick={() => {
                updateFormData("art", "B");
              }}
            >
              Brand
            </Button>
            <Button
              variant={formData?.art === "H" ? "contained" : "outlined"}
              onClick={() => {
                updateFormData("art", "H");
              }}
            >
              Hilfeleistung
            </Button>
          </ButtonGroup>
        </Stack>

        <TextField
          variant="outlined"
          name="nummer"
          label="Einsatznummer"
          error={errors?.nummer?.hasError}
          helperText={errors?.nummer?.message}
          type="number"
          sx={{ width: { xs: "100%", sm: "50%" } }}
          value={formData?.nummer || ""}
          onChange={handleChange}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">{formData?.art}</InputAdornment>
            ),
          }}
        />
      </FormRow>

      <FormRow>
        <DateTimePicker
          id="start"
          label="Alarmzeit"
          format="DD/MM/YYYY HH:mm"
          slotProps={{
            textField: {
              error: errors?.start?.hasError,
              helperText: errors?.start?.message,
              onBlur: () => updateFormData("start", dayjs(startTime).toJSON()),
            },
          }}
          sx={{ width: { xs: "100%", sm: "50%" } }}
          value={startTime}
          onChange={(value) => setStartTime(value)}
          onAccept={() => updateFormData("start", dayjs(startTime).toJSON())}
        />
        <DateTimePicker
          id="ende"
          label="Einsatzende"
          format="DD/MM/YYYY HH:mm"
          slotProps={{
            textField: {
              error: errors?.ende?.hasError,
              helperText: errors?.ende?.message,
              onBlur: () => updateFormData("ende", dayjs(endTime).toJSON()),
            },
          }}
          sx={{ width: { xs: "100%", sm: "50%" } }}
          value={endTime}
          onChange={(value) => setEndTime(value)}
          onAccept={() => updateFormData("ende", dayjs(endTime).toJSON())}
        />
      </FormRow>

      <FormRow>
        <TextField
          label="Alarmierte Einheit"
          id="einheit"
          name="einheit"
          select
          error={errors?.einheit?.hasError}
          helperText={errors?.einheit?.message}
          value={formData?.einheit || ""}
          onChange={handleChange}
          sx={{ width: { xs: "100%", sm: "50%" } }}
        >
          {einheiten.map((einheit) => (
            <MenuItem key={einheit} value={einheit}>
              {einheit}
            </MenuItem>
          ))}
        </TextField>
        <TextField
          variant="outlined"
          name="stichwort"
          label="Stichwort"
          error={errors?.stichwort?.hasError}
          helperText={errors?.stichwort?.message}
          type="text"
          sx={{ width: { xs: "100%", sm: "50%" } }}
          value={formData?.stichwort || ""}
          onChange={handleChange}
        />
      </FormRow>

      <FormRow>
        <TextField
          variant="outlined"
          name="ort"
          label="Objekt / Ort"
          error={errors?.ort?.hasError}
          helperText={errors?.ort?.message}
          type="text"
          sx={{ width: { xs: "100%", sm: "50%" } }}
          value={formData?.ort || ""}
          onChange={handleChange}
        />

        <Stack
          direction="row"
          spacing={2}
          alignItems="center"
          justifyContent="flex-end"
          sx={{ width: { xs: "100%", sm: "50%" } }}
        >
          <Typography variant="h6">In Bereitschaft</Typography>
          <Switch
            name="inBereitschaft"
            checked={formData?.inBereitschaft}
            onChange={handleSwitchChange}
          />
        </Stack>
      </FormRow>

      {!formData?.inBereitschaft && (
        <>
          <FormRow>
            <TextField
              label="Fahrzeug"
              id="fahrzeug"
              name="fahrzeug"
              select
              error={errors?.fahrzeug?.hasError}
              helperText={errors?.fahrzeug?.message}
              value={formData?.fahrzeug || ""}
              onChange={handleChange}
              sx={{ width: { xs: "100%", sm: "50%" } }}
            >
              {fahrzeuge.map((fahrzeug) => (
                <MenuItem key={fahrzeug} value={fahrzeug}>
                  {fahrzeug}
                </MenuItem>
              ))}
            </TextField>

            <TextField
              label="Funktion"
              id="funktion"
              name="funktion"
              select
              error={errors?.funktion?.hasError}
              helperText={errors?.funktion?.message}
              value={formData?.funktion || ""}
              onChange={handleChange}
              sx={{ width: { xs: "100%", sm: "50%" } }}
            >
              {funktionen.map((funktion) => (
                <MenuItem key={funktion} value={funktion}>
                  {funktion}
                </MenuItem>
              ))}
            </TextField>
          </FormRow>

          <FormRow>
            <Stack
              direction="row"
              spacing={2}
              alignItems="center"
              sx={{ width: { xs: "100%", sm: "50%" } }}
            >
              <Typography variant="h6">PA angeschlossen</Typography>
              <Switch
                name="pa"
                checked={formData?.pa}
                onChange={handleSwitchChange}
              />
            </Stack>

            {formData?.pa && (
              <TextField
                variant="outlined"
                name="paDauer"
                label="Dauer unter PA"
                error={errors?.paDauer?.hasError}
                helperText={errors?.paDauer?.message}
                type="number"
                sx={{ width: { xs: "100%", sm: "50%" } }}
                value={formData?.paDauer || ""}
                onChange={handleChange}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">min</InputAdornment>
                  ),
                }}
              />
            )}
          </FormRow>
        </>
      )}

      <FormRow>
        <Button
          variant="outlined"
          color="primary"
          size="large"
          onClick={resetStatesAndErrors}
        >
          Eingaben löschen
        </Button>
        <Button variant="contained" color="primary" size="large" type="submit">
          Speichern
        </Button>
      </FormRow>
    </form>
  );
}

export default AlarmForm;
