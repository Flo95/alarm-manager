import { useState, useEffect } from "react";
import {
  Box,
  Container,
  Typography,
  Snackbar,
  Alert,
  AlertTitle,
} from "@mui/material";

import { useDispatch, useSelector } from "react-redux";
import { addAlarmAsync } from "../store/actions";
import { selectStatus, selectError, resetStatus } from "../store/alarmSlice";

import Navbar from "../components/Navbar";
import AlarmForm from "../components/AlarmForm";

function NewAlarm() {
  const dispatch = useDispatch();
  const StoreStatus = useSelector(selectStatus);
  const StoreError = useSelector(selectError);

  const [snackbar, setSnackbar] = useState({
    open: false,
    title: "",
    message: "",
    severity: "success",
  });

  useEffect(() => {
    if (StoreStatus === "addingSucceeded") {
      setSnackbar({
        open: true,
        title: "Einsatz angelegt",
        message: "Einsatz wurde angelegt.",
        severity: "success",
      });
      dispatch(resetStatus());
    }
    if (StoreStatus === "addingFailed") {
      console.error(StoreError);
      setSnackbar({
        open: true,
        title: "Warnung",
        message: "Einsatz konnte nicht angelegt werden.",
        severity: "error",
      });
      dispatch(resetStatus());
    }
  }, [StoreStatus, StoreError, dispatch]);

  function handleSubmit(newAlarm) {
    dispatch(addAlarmAsync(newAlarm));
  }

  return (
    <>
      <Navbar />
      <Box component="main">
        <Container maxWidth="lg" sx={{ mt: 3 }}>
          <Typography variant="h4" color="grey">
            Neuen Einsatz anlegen
          </Typography>

          <AlarmForm onSubmit={(newAlarm) => handleSubmit(newAlarm)} />

          <Snackbar
            open={snackbar.open}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
            autoHideDuration={6000}
            onClose={() => setSnackbar({ ...snackbar, open: false })}
          >
            <Alert
              onClose={() => setSnackbar({ ...snackbar, open: false })}
              severity={snackbar.severity}
              variant="filled"
              sx={{ width: "100%" }}
            >
              <AlertTitle>{snackbar.title}</AlertTitle>
              {snackbar.message}
            </Alert>
          </Snackbar>
        </Container>
      </Box>
    </>
  );
}

export default NewAlarm;
