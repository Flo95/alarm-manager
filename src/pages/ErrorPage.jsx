import { Box, Typography } from "@mui/material";
import { useRouteError, Link } from "react-router-dom";

function ErrorPage() {
  console.error(useRouteError());

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
      }}
    >
      <Typography
        variant="h2"
        color="grey"
        sx={{ mb: 4, borderBottom: "2px solid red", px: 2 }}
      >
        EinsatzManager
      </Typography>
      <Typography variant="h4" color="black" sx={{ mb: 6 }}>
        Seite nicht gefunden!
      </Typography>
      <Link to="/">
        <Typography>Zum Login</Typography>
      </Link>
    </Box>
  );
}

export default ErrorPage;
