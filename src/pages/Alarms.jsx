import { useEffect, useState } from "react";
import {
  Box,
  Container,
  Typography,
  Snackbar,
  Alert,
  AlertTitle,
} from "@mui/material";

import Navbar from "../components/Navbar";
import CardContainer from "../components/CardContainer";
import AlarmCard from "../components/AlarmCard";

import { useSelector, useDispatch } from "react-redux";
import {
  selectAlarms,
  selectStatus,
  selectError,
  resetStatus,
} from "../store/alarmSlice";

import dayjs from "dayjs";

function sortAlarmsByDate(alarms) {
  return alarms.slice().sort((a, b) => {
    const dateA = dayjs(a.start);
    const dateB = dayjs(b.start);
    return dateB.diff(dateA, "second") < 0 ? -1 : 1;
  });
}

function Alarms() {
  const dispatch = useDispatch();
  const alarmsFromStore = useSelector(selectAlarms);
  const StoreStatus = useSelector(selectStatus);
  const StoreError = useSelector(selectError);

  const [snackbar, setSnackbar] = useState({
    open: false,
    title: "",
    message: "",
    severity: "success",
  });

  useEffect(() => {
    if (StoreStatus === "deletingSucceeded") {
      setSnackbar({
        open: true,
        title: "Einsatz gelöscht",
        message: "Einsatz wurde gelöscht.",
        severity: "success",
      });
      dispatch(resetStatus());
    }
    if (StoreStatus === "deletingFailed") {
      console.error(StoreError);
      setSnackbar({
        open: true,
        title: "Warnung",
        message: "Einsatz konnte nicht gelöscht werden.",
        severity: "error",
      });
      dispatch(resetStatus());
    }
  }, [StoreStatus, StoreError, dispatch]);

  const sortedAlarms = sortAlarmsByDate(alarmsFromStore);

  return (
    <>
      <Navbar />
      <Box component="main">
        <Container maxWidth="lg" sx={{ mt: 3 }}>
          <Typography variant="h4" color="grey" sx={{ mb: 2 }}>
            Einsatzübersicht
          </Typography>

          {sortedAlarms.length > 0 ? (
            <CardContainer>
              {sortedAlarms.map((alarm) => (
                <AlarmCard key={alarm.storeId} alarm={alarm} />
              ))}
            </CardContainer>
          ) : (
            <Typography variant="h5" color="black" fontStyle="italic">
              Keine Einträge gefunden
            </Typography>
          )}

          <Snackbar
            open={snackbar.open}
            anchorOrigin={{ vertical: "top", horizontal: "center" }}
            autoHideDuration={6000}
            onClose={() => setSnackbar({ ...snackbar, open: false })}
          >
            <Alert
              onClose={() => setSnackbar({ ...snackbar, open: false })}
              severity={snackbar.severity}
              variant="filled"
              sx={{ width: "100%" }}
            >
              <AlertTitle>{snackbar.title}</AlertTitle>
              {snackbar.message}
            </Alert>
          </Snackbar>
        </Container>
      </Box>
    </>
  );
}

export default Alarms;
