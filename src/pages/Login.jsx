import { useEffect } from "react";
import { Box, Button, Typography } from "@mui/material";
import { Link } from "react-router-dom";

import { useSelector, useDispatch } from "react-redux";
import { selectStatus } from "../store/alarmSlice";
import { fetschAlarmsAsync } from "../store/actions";

function Login() {
  const dispatch = useDispatch();
  const StoreStatus = useSelector(selectStatus);

  useEffect(() => {
    if (StoreStatus === "idle") {
      dispatch(fetschAlarmsAsync());
    }
  }, [StoreStatus, dispatch]);

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
      }}
    >
      <Typography
        variant="h2"
        color="grey"
        sx={{ mb: 4, borderBottom: "2px solid red", px: 2 }}
      >
        EinsatzManager
      </Typography>
      <Link to="/uebersicht">
        <Button variant="contained" size="large">
          Login
        </Button>
      </Link>
    </Box>
  );
}

export default Login;
